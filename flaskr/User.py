from flask import Flask
from werkzeug.security import generate_password_hash, check_password_hash
import Team
import Task
import re, sys, os

class User():
    def __init__(self, username, password):
        self.username = username
        self.setPassword(password)
        self.teams = []

    def setPassword(self, password):
        self.passHash = generate_password_hash(password)

    def checkPassword(self, password):
        return check_password_hash(self.passHash, password)

    def checkUsername(self, username):
        return self.username == username

    def getUsername(self):
        return self.username

    def getPassword(self):
        return self.passHash

    def updateUsername(self, newName):
        self.username = newName

    def updatePassword(self, newPass):
        self.password = newPass

    def addTeam(self,team):
        self.teams.append(team)

    def getTeams(self):
        return self.teams