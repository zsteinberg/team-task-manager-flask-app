from flask import Flask
import User, Team
import re, sys, os
from datetime import datetime

class Task():
    def __init__(self, taskName):
       self.taskName = taskName
       self.state = 0
       self.users = []

    def addUser(self, user):
        self.users.append(user) 
    
    def getUsers(self):
        return self.users

    def stateUpdate(self, state):
        self.state = state

    def getState(self):
        return self.state
    
    def editName(self, newName):
        self.taskName = newName

    



