from flask import Flask, jsonify, request, json, render_template, session
from flask_bootstrap import Bootstrap
from flask_nav import Nav
from flask_nav.elements import Navbar, Subgroup, View, Link, Text, Separator
from werkzeug.security import check_password_hash
from pymongo import MongoClient
from flask_pymongo import PyMongo
from flask_user import login_required
from datetime import datetime
from User import User
from Task import Task
from Team import Team

app = Flask(__name__)
app.secret_key = '_5#y2L"F4Q8z\n\xec]/'
Bootstrap(app)
nav = Nav(app)

# Connect to Mongo Database
app.config['MONGO_DBNAME'] = 'task_man'
app.config['MONGO_URI'] = 'mongodb://localhost:27013/task_man'
app.config['JWT_SECRET_KEY'] = 'secret'

client = MongoClient('localhost', 27013)
db = client['task_man']


mongo = PyMongo(app)

# NavBar Rendering
@nav.navigation()
def mynavbar():
    if 'username' in session:
        log_btn = Text('Welcome: '+session['username'])
        add_btn = View('Logout', 'logout')
    else:
        log_btn = View('Login', 'loginPage')
        add_btn = View('Sign Up', 'signUpPrompt')

    return Navbar(
        'Team Task Manager',
        View('Home', 'index'),
        View('Teams', 'teams'),
        View('Tasks', 'tasks'),
        View('Notifications', 'nots'),
        View('Reports', 'reports'),
        log_btn, add_btn
    )

# Errors
@app.errorhandler(400)
def badRequest(e):
    return render_template('400.html'), 400
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(405)
def method_not_allowed(e):
    return render_template('405.html'), 405

# Routes
@app.route('/')
def index():
    if 'username' in session:
        return render_template('home.html',
                               username=session['username'],
                               status=True
                               )
    else:
        return render_template('home.html', status=False)


@app.route('/teams')
def teams():
    if 'username' in session:
        return render_template('teams.html',
                               username=session['username'],
                               status=True,
                               teams=teams,
                               )
    else:
        return render_template('teams.html', status=False)


@app.route('/tasks')
def tasks():
    if 'username' in session:
        return render_template('tasks.html',
                               username=session['username'],
                               status=True
                               )
    else:
        return render_template('tasks.html', status=False)


@app.route('/notifications')
def nots():
    if 'username' in session:
        collection = db['users']
        user = collection.find_one({'username': session['username']})
        tasks = user['tasks']
        return render_template('notifications.html',
                               username=session['username'],
                               status=True,
                               tasks=tasks
                               )
    else:
        return render_template('notifications.html', status=False)


@app.route('/reports')
def reports():
    if 'username' in session:
        collection = db['tasks']
        tasks = collection.find({'user': session['username']})
        return render_template('reports.html',
                               tasks=tasks,
                               username=session['username'],
                               status=True
                               )
    else:
        return render_template('reports.html', status=False)


@app.route('/signUpPrompt')
def signUpPrompt():
    return render_template('signUpPrompt.html', status=False)

# Add User to the database
@app.route('/signUp/', methods=["POST"])
def addUser():
    collection = db['users']
    username = request.form['username']
    password = request.form['password']
    confPass = request.form['confirmPassword']
    newUser = User(username, password)
    status = True
    passStat = True
    if collection.find_one({'username': newUser.username}):
        status = False
    elif password != confPass:
        passStat = False
    else:
        teams = []
        notifications = []
        tasks = []
        # User Schema
        collection.insert_one({
            'username': newUser.username,
            'password': newUser.passHash,
            'teams': teams,
            'notifications': notifications,
            'tasks': tasks
        })

    return render_template('postSignUp.html',
                           status=status,
                           passStat=passStat,
                           username=newUser.username)


@app.route('/loginPage')
def loginPage():
    return render_template('loginPrompt.html')

# Login Verification
@app.route('/login/', methods=['POST'])
def login():
    collection = db['users']
    username = request.form['username']
    password = request.form['password']
    verify = collection.find_one({'username': username})

    if verify:
        dbPass = verify['password']
        if check_password_hash(dbPass, password):
            session['username'] = username
            return render_template('postLogin.html',
                                   username=session['username'],
                                   status=True
                                   )
    else:
        return render_template('postLogin.html', status=False)


@app.route('/logout/')
def logout():
    session.pop('username', None)
    return render_template('loggedOut.html')


@app.route('/createTeamPrompt')
def setTeam():
    return render_template('teamPrompt.html')

# Inserting Team into Mongo Database
@app.route('/createTeam/', methods=['POST'])
def createTeam():
    collection = db['teams']
    users = db['users']
    name = request.form['name']
    descr = request.form['description']
    verify = collection.find_one({'name': name})

    if verify:
        status = False
    else:
        # Team Schema
        collection.insert_one({
            'name': name,
            'admin': session['username'],
            'description': descr,
            'members': [session['username']],
            'tasks': []
        })
        users.update_one({'username': session['username']}, {
                         "$addToSet": {'teams': name}})
        status = True
    return render_template('teamCreated.html',
                           admin=session['username'],
                           team=name,
                           status=status
                           )


@app.route('/createTaskPromptTeams')
def createTask():
    teamsArr = db['users']
    teams = teamsArr.find_one({'username': session['username']})
    array = teams['teams']
    return render_template('taskPrompt.html', teams=array)


@app.route('/createTaskPromptUser')
def createTaskUser():
    return render_template('taskPromptUser.html', username=session['username'])

# Insert Team Task Into Mongo
@app.route('/createTask/', methods=['POST'])
def setTask():
    collection = db['tasks']
    point = db['teams']
    name = request.form['name']
    descr = request.form['description']
    team = request.form['teams']
    thisTeam = point.find_one({'name': team})
    point.update_one({'_id': thisTeam['_id']}, {"$addToSet": {'tasks': name}})

    # Task Schema
    collection.insert_one({
        'name': name,
        'description': descr,
        'user': None,
        'team': team,
        'progress': 0
    })

    verify = collection.find_one({'name': name})
    if verify:
        status = True
    else:
        status = False
    return render_template('taskCreated.html',
                           task=name,
                           status=status
                           )

# Insert Private Task Into Mongo
@app.route('/createTaskUser/', methods=['POST'])
def setTaskUser():
    collection = db['tasks']
    user = db['users']
    name = request.form['name']
    descr = request.form['description']
    user.update_one({'username': session['username']},
                    {"$addToSet": {'tasks': {'task': name}}})

    collection.insert_one({
        'name': name,
        'description': descr,
        'user': session['username'],
        'team': "Private",
        'progress': 0
    })
    verify = collection.find_one({'name': name})
    if verify:
        status = True
    else:
        status = False
    return render_template('taskCreated.html',
                           task=name,
                           status=status
                           )


# View List of Teams
@app.route('/viewTeams')
def viewTeams():
    collection = db['users']
    userP = collection.find_one({'username': session['username']})
    user = userP['username']
    teams = userP['teams']
    return render_template('viewTeams.html', teams=teams, user=user)

# View Tasks and Users of an Individual Team
@app.route('/teamPage/', methods=['POST'])
def viewTeam():
    session.pop('team',None)
    team = request.form['teams']
    session['team'] = team
    collection = db['teams']
    teamFind = collection.find_one({'name': team})
    users = teamFind['members']
    admin = teamFind['admin']
    globeUsers = db['users']
    globe = globeUsers.find({},{'username': 1})
    allUsers = []
    for user in globe:
        allUsers.append(user['username'])
    return render_template('teamPage.html',
                           teamUsers=users,
                           admin=admin,
                           name=session['team'],
                           allUsers=allUsers,
                           username=session['username']
                           )


@app.route('/viewUserTasks')
def viewUserTask():
    # userCol = db['users']
    collection = db['tasks']
    # user = userCol.find_one({'username': session['username']})
    tasks = collection.find({'user': session['username']})
    return render_template('viewUserTasks.html',
                           tasks=tasks,
                           user=session['username']
                           )


@app.route('/addTeammate/', methods=['POST'])
def addTeamMember():
    newMember = request.form['teamMember']
    teamDB = request.form['teams']
    userDB = request.form['users']
    return render_template('addTeammate.html',
                           newMember=newMember,
                           teamName=session['team']
                           )


@app.route('/updateTeamTask/', methods=['POST'])
def updateTeamTask():
    return render_template('updateTeamTask.html')


@app.route('/updateUserTaskPrompt/', methods=['POST'])
def updateUserTask():
    return render_template('taskUpdated.html')


if __name__ == '__main__':
    app.run(debug=True)
